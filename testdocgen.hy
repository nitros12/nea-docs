(import pytest
        os
        sys
        io
        argparse
        inspect
        [importlib [import-module]]
        [tabulate [tabulate]])


(defn get-tests-for-module [name module]
  "Extract the test cases of a module"
  (map second
       (inspect.getmembers module
                           (fn [obj] (and (hasattr obj "__module__")
                                          (.startswith obj.--module-- name)
                                          (obj.--name--.startswith "test"))))))


(defn format-test [fn escape]
  (setv name (escape fn.__name__)
        features (.join ", " (.values (getattr fn "_features" {})))
        desc (.join " " (map str.strip (fn.__doc__.splitlines)))
        line (str (or (getattr fn "_line" None) fn.__code__.co_firstlineno)))
  (, name desc features line))


(defmain [&rest _]
  (setv args (.parse-args
               (doto (argparse.ArgumentParser :description "Generate test case table for some tests.")
                     (.add-argument "file"
                                    :help "file to get tests from")
                     (.add-argument "-f" "--folder"
                                    :default (os.getcwd)
                                    :help "folder to set cwd to")
                     (.add-argument "-e" "--escaper"
                                    :default "="
                                    :help "character to wrap names with")
                     (.add-argument "-o" "--out"
                                    :nargs "?"
                                    :type (argparse.FileType "w")
                                    :default sys.stdout
                                    :help "output file, defaults to stdout")
                     (.add-argument "--format"
                                    :default "orgtbl"
                                    :help "table format to use, defaults to 'orgtbl'"))))
  (os.chdir args.folder)
  (sys.path.append args.folder)
  (setv module (import-module args.file)
        tests (get-tests-for-module args.file module)
        results (list
                  (map (fn [t] (format-test
                                 t
                                 (fn [v]
                                   (.format "{e}{f}{e}"
                                            :e args.escaper
                                            :f v))))
                     tests)))
  (args.out.write (tabulate results
                            :headers (, "Test name" "Test description" "Features tested" "Line number")
                            :tablefmt args.format)))
