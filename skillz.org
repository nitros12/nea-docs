#+TITLE: Skills used in NEA project
#+AUTHOR: Ben Simms
#+EMAIL: ben@bensimms.moe
#+BIND: org-odt-preferred-output-format "pdf"
#+OPTIONS: prop:t

#+NAME: export-set
#+BEGIN_SRC emacs-lisp :exports none
(setq org-export-directory (expand-file-name "./renders")
      org-odt-preferred-output-format "pdf"
      org-src-fontify-natively t)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (plantuml . t)
   (ditaa . t)))
(require 'irony)
#+END_SRC

#+RESULTS: export-set
: irony

#+CALL: export-set() :exports none

- Hash tables :: Python dictionaries are used to store the name of variables to
     their info objects inside the compiler, aswell as in the register allocator
     to keep track of register states. (page 89, class AllocationState)
- Abstract/ Interface Classes and mixins :: Used to create interfaces for being
     used in many objects that should support the functionality described in the
     interface. (page 101, class IdentifierScope)
- Sets :: A set is used to keep track of the current active registers in the
     register allocator. (page 89, class AllocationState)
- Enums :: Enumarable objects are used in the register allocator to mark the
     allocation state of a register. (page 89, class RegisterState)
- Trees :: The AST of the compiler is a tree structure (Abstract syntax tree).
- Queues :: The async compile loop uses a queue to keep processing compilation
     objects until all are complete or halted. (section 3.1.4 - page 53)
- Complex oop model :: My IR generation code makes strong use of inheritance to
     minimise repeated code and to ensure a consistent interface for other AST
     nodes, composition is used to have the AST nodes take on a tree format.
     Each compile method is an example of OOP polymorphism/ interfaces. (3.1.3.6 - page 46)
- Complex oop model :: Another example of a complex oop model is in my emitter
     objects which I use for desugaring (3.1.5 - page 57)
- Tree traversal :: The ir generation itself is a recursive tree traversal,
     statements and expression nodes contain child AST nodes that have their
     compilation methods invoked when compiled by the parent node.
- Complex user defined algorithms :: The asynchronous compilation algorithm
     leverages python's coroutines to manage processing each compilation unit,
     pausing and resuming coroutines when variables become defined (3.1.4). The
     register allocator is another user defined algorithm that scans IR nodes
     and allocates registers for them (3.1.6).
- Dynamic generation of objects :: The AST node builder (3.1.2.2 - pagfe 28) and
     IR -> HW instruction converter (3.1.8 - page 60) is an example of dynamic
     generation of objects.
- Good use of language features :: The async compilation makes use of python
     coroutines to abstract out the presence of the compilation being
     asynchronous from the compilation methods, this means the compilation
     functions of each AST node is able to be written assuming that all
     variables requested will exist, and no special handling cases will have to
     be written into them.
