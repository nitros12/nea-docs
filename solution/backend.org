* =assemble.py=
Contains functions to process and assemble IR code, all pre and post-processing
that needs to happen to IR and machine instructions happen here.

#+CAPTION: =wewcompiler/backend/rustvm/assemble.py=
#+INCLUDE: "../A-Compiler/wewcompiler/backend/rustvm/assemble.py" src python -n
* =desugar.py=
Contains the logic for desugaring IR objects, both stage 1 and stage 2 are
located here.

#+CAPTION: =wewcompiler/1=
#+INCLUDE: "../A-Compiler/wewcompiler/backend/rustvm/desugar.py" src python -n
* =encoder.py=
Contains definitions of all machine instructions and registers for the virtual
machine. Also contains a desugaring method to translate IR objects into machine
instructions. Functions for packing instructions and parameters into bytes are
also located here.

#+CAPTION: =wewcompiler/backend/rustvm/encoder.py=
#+INCLUDE: "../A-Compiler/wewcompiler/backend/rustvm/encoder.py" src python -n
* =register_alloc.py=
This file contains the source code for the register allocator, the algorithm is described in the design section.

#+CAPTION: =wewcompiler/backend/rustvm/register_allocate.py=
#+INCLUDE: "../A-Compiler/wewcompiler/backend/rustvm/register_allocate.py" src python -n
* =stdlib.wew=
Contains the standard library of my language, this also serves as an example of
the language's syntax and features.

#+CAPTION: =wewcompiler/backend/rustvm/stdlib.wew=
#+INCLUDE: "../A-Compiler/wewcompiler/backend/rustvm/stdlib.wew" src rust -n
